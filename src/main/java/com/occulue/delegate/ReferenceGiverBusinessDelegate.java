/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.*;

/**
 * ReferenceGiver business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of ReferenceGiver related services in the case of a ReferenceGiver business related service failing.</li>
 * <li>Exposes a simpler, uniform ReferenceGiver interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill ReferenceGiver business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class ReferenceGiverBusinessDelegate 
extends BaseBusinessDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public ReferenceGiverBusinessDelegate() 
	{
	}

        
   /**
	* ReferenceGiver Business Delegate Factory Method
	*
	* Returns a singleton instance of ReferenceGiverBusinessDelegate().
	* All methods are expected to be self-sufficient.
	*
	* @return 	ReferenceGiverBusinessDelegate
	*/
	public static ReferenceGiverBusinessDelegate getReferenceGiverInstance()
	{
	    if ( singleton == null )
	    {
	    	singleton = new ReferenceGiverBusinessDelegate();
	    }
	    
	    return( singleton );
	}
 
    /**
     * Method to retrieve the ReferenceGiver via an ReferenceGiverPrimaryKey.
     * @param 	key
     * @return 	ReferenceGiver
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException 
     */
    public ReferenceGiver getReferenceGiver( ReferenceGiverPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "ReferenceGiverBusinessDelegate:getReferenceGiver - ";
        if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        ReferenceGiver returnBO = null;
                
        ReferenceGiverDAO dao = getReferenceGiverDAO();
        
        try
        {
            returnBO = dao.findReferenceGiver( key );
        }
        catch( Exception exc )
        {
            String errMsg = "ReferenceGiverBusinessDelegate:getReferenceGiver( ReferenceGiverPrimaryKey key ) - unable to locate ReferenceGiver with key " + key.toString() + " - " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferenceGiverDAO( dao );
        }        
        
        return returnBO;
    }


    /**
     * Method to retrieve a collection of all ReferenceGivers
     *
     * @return 	ArrayList<ReferenceGiver> 
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<ReferenceGiver> getAllReferenceGiver() 
    throws ProcessingException
    {
    	String msgPrefix				= "ReferenceGiverBusinessDelegate:getAllReferenceGiver() - ";
        ArrayList<ReferenceGiver> array	= null;
        
        ReferenceGiverDAO dao = getReferenceGiverDAO();
    
        try
        {
            array = dao.findAllReferenceGiver();
        }
        catch( Exception exc )
        {
            String errMsg = msgPrefix + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferenceGiverDAO( dao );
        }        
        
        return array;
    }

   /**
    * Creates the provided BO.
    * @param		businessObject 	ReferenceGiver
    * @return       ReferenceGiver
    * @exception    ProcessingException
    * @exception	IllegalArgumentException
    */
	public ReferenceGiver createReferenceGiver( ReferenceGiver businessObject )
    throws ProcessingException, IllegalArgumentException
    {
		String msgPrefix = "ReferenceGiverBusinessDelegate:createReferenceGiver - ";
		
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        // return value once persisted
        ReferenceGiverDAO dao  = getReferenceGiverDAO();
        
        try
        {
            businessObject = dao.createReferenceGiver( businessObject );
        }
        catch (Exception exc)
        {
            String errMsg = "ReferenceGiverBusinessDelegate:createReferenceGiver() - Unable to create ReferenceGiver" + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferenceGiverDAO( dao );
        }        
        
        return( businessObject );
        
    }

   /**
    * Saves the underlying BO.
    * @param		businessObject		ReferenceGiver
    * @return       what was just saved
    * @exception    ProcessingException
    * @exception  	IllegalArgumentException
    */
    public ReferenceGiver saveReferenceGiver( ReferenceGiver businessObject ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "ReferenceGiverBusinessDelegate:saveReferenceGiver - ";
    	
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
                
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ReferenceGiverPrimaryKey key = businessObject.getReferenceGiverPrimaryKey();
                    
        if ( key != null )
        {
            ReferenceGiverDAO dao = getReferenceGiverDAO();

            try
            {                    
                businessObject = (ReferenceGiver)dao.saveReferenceGiver( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "ReferenceGiverBusinessDelegate:saveReferenceGiver() - Unable to save ReferenceGiver" + exc;
                LOGGER.warning( errMsg );
                throw new ProcessingException( errMsg  );
            }
            finally
            {
                releaseReferenceGiverDAO( dao );
            }
            
        }
        else
        {
            String errMsg = "ReferenceGiverBusinessDelegate:saveReferenceGiver() - Unable to create ReferenceGiver due to it having a null ReferenceGiverPrimaryKey."; 
            
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
		        
        return( businessObject );
        
    }
   
   /**
    * Deletes the associatied value object using the provided primary key.
    * @param		key 	ReferenceGiverPrimaryKey    
    * @exception 	ProcessingException
    */
    public void delete( ReferenceGiverPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {    	
    	String msgPrefix = "ReferenceGiverBusinessDelegate:saveReferenceGiver - ";
    	
		if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        ReferenceGiverDAO dao  = getReferenceGiverDAO();

        try
        {                    
            dao.deleteReferenceGiver( key );
        }
        catch (Exception exc)
        {
            String errMsg = msgPrefix + "Unable to delete ReferenceGiver using key = "  + key + ". " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferenceGiverDAO( dao );
        }
        		
        return;
    }
        

// business methods
    /**
     * Returns the ReferenceGiver specific DAO.
     *
     * @return      ReferenceGiver DAO
     */
    public ReferenceGiverDAO getReferenceGiverDAO()
    {
        return( new com.occulue.dao.ReferenceGiverDAO() ); 
    }

    /**
     * Release the ReferenceGiverDAO back to the FrameworkDAOFactory
     */
    public void releaseReferenceGiverDAO( com.occulue.dao.ReferenceGiverDAO dao )
    {
        dao = null;
    }

// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
// ~AIB
     
//************************************************************************
// Attributes
//************************************************************************

   /**
    * Singleton instance
    */
    protected static ReferenceGiverBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(ReferenceGiver.class.getName());
    
}



