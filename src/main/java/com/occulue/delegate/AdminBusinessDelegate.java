/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.*;

/**
 * Admin business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Admin related services in the case of a Admin business related service failing.</li>
 * <li>Exposes a simpler, uniform Admin interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Admin business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class AdminBusinessDelegate 
extends BaseBusinessDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public AdminBusinessDelegate() 
	{
	}

        
   /**
	* Admin Business Delegate Factory Method
	*
	* Returns a singleton instance of AdminBusinessDelegate().
	* All methods are expected to be self-sufficient.
	*
	* @return 	AdminBusinessDelegate
	*/
	public static AdminBusinessDelegate getAdminInstance()
	{
	    if ( singleton == null )
	    {
	    	singleton = new AdminBusinessDelegate();
	    }
	    
	    return( singleton );
	}
 
    /**
     * Method to retrieve the Admin via an AdminPrimaryKey.
     * @param 	key
     * @return 	Admin
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException 
     */
    public Admin getAdmin( AdminPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "AdminBusinessDelegate:getAdmin - ";
        if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        Admin returnBO = null;
                
        AdminDAO dao = getAdminDAO();
        
        try
        {
            returnBO = dao.findAdmin( key );
        }
        catch( Exception exc )
        {
            String errMsg = "AdminBusinessDelegate:getAdmin( AdminPrimaryKey key ) - unable to locate Admin with key " + key.toString() + " - " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseAdminDAO( dao );
        }        
        
        return returnBO;
    }


    /**
     * Method to retrieve a collection of all Admins
     *
     * @return 	ArrayList<Admin> 
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<Admin> getAllAdmin() 
    throws ProcessingException
    {
    	String msgPrefix				= "AdminBusinessDelegate:getAllAdmin() - ";
        ArrayList<Admin> array	= null;
        
        AdminDAO dao = getAdminDAO();
    
        try
        {
            array = dao.findAllAdmin();
        }
        catch( Exception exc )
        {
            String errMsg = msgPrefix + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseAdminDAO( dao );
        }        
        
        return array;
    }

   /**
    * Creates the provided BO.
    * @param		businessObject 	Admin
    * @return       Admin
    * @exception    ProcessingException
    * @exception	IllegalArgumentException
    */
	public Admin createAdmin( Admin businessObject )
    throws ProcessingException, IllegalArgumentException
    {
		String msgPrefix = "AdminBusinessDelegate:createAdmin - ";
		
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        // return value once persisted
        AdminDAO dao  = getAdminDAO();
        
        try
        {
            businessObject = dao.createAdmin( businessObject );
        }
        catch (Exception exc)
        {
            String errMsg = "AdminBusinessDelegate:createAdmin() - Unable to create Admin" + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseAdminDAO( dao );
        }        
        
        return( businessObject );
        
    }

   /**
    * Saves the underlying BO.
    * @param		businessObject		Admin
    * @return       what was just saved
    * @exception    ProcessingException
    * @exception  	IllegalArgumentException
    */
    public Admin saveAdmin( Admin businessObject ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "AdminBusinessDelegate:saveAdmin - ";
    	
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
                
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        AdminPrimaryKey key = businessObject.getAdminPrimaryKey();
                    
        if ( key != null )
        {
            AdminDAO dao = getAdminDAO();

            try
            {                    
                businessObject = (Admin)dao.saveAdmin( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "AdminBusinessDelegate:saveAdmin() - Unable to save Admin" + exc;
                LOGGER.warning( errMsg );
                throw new ProcessingException( errMsg  );
            }
            finally
            {
                releaseAdminDAO( dao );
            }
            
        }
        else
        {
            String errMsg = "AdminBusinessDelegate:saveAdmin() - Unable to create Admin due to it having a null AdminPrimaryKey."; 
            
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
		        
        return( businessObject );
        
    }
   
   /**
    * Deletes the associatied value object using the provided primary key.
    * @param		key 	AdminPrimaryKey    
    * @exception 	ProcessingException
    */
    public void delete( AdminPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {    	
    	String msgPrefix = "AdminBusinessDelegate:saveAdmin - ";
    	
		if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        AdminDAO dao  = getAdminDAO();

        try
        {                    
            dao.deleteAdmin( key );
        }
        catch (Exception exc)
        {
            String errMsg = msgPrefix + "Unable to delete Admin using key = "  + key + ". " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseAdminDAO( dao );
        }
        		
        return;
    }
        

// business methods
    /**
     * Returns the Admin specific DAO.
     *
     * @return      Admin DAO
     */
    public AdminDAO getAdminDAO()
    {
        return( new com.occulue.dao.AdminDAO() ); 
    }

    /**
     * Release the AdminDAO back to the FrameworkDAOFactory
     */
    public void releaseAdminDAO( com.occulue.dao.AdminDAO dao )
    {
        dao = null;
    }

// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
// ~AIB
     
//************************************************************************
// Attributes
//************************************************************************

   /**
    * Singleton instance
    */
    protected static AdminBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(Admin.class.getName());
    
}



