/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.*;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;

import com.occulue.bo.*;

/**
 * Base class for application Value Objects.
 * 
 * @author dev@realmethods.com
 */ 
public abstract class Base
{
	/**
	 * Default Constructor
	 */
	protected Base()
	{
		super();
	}
	
    public abstract String getIdentity();
    public abstract String getObjectType();
    
    public boolean equals( Object object )
    { return true; }
    
    public void copyShallow( Object object )
    {
    	this.objectId = ((Base)object).objectId;
    }
    
    public void copy( Object object )
    {
    	this.objectId = ((Base)object).objectId;
    }

    public ObjectId getObjectId()
    {
    	return objectId;
    }
    
    public void setObjectId( ObjectId objectId )
    {
    	this.objectId = objectId;
    }
    
    // attributes
    @Id
    protected ObjectId objectId;	// MongoDB unique identified
	
}


