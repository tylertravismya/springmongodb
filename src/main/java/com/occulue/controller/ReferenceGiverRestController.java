/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity ReferenceGiver.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/ReferenceGiver")
public class ReferenceGiverRestController extends BaseSpringRestController
{

    /**
     * Handles saving a ReferenceGiver BO.  if not key provided, calls create, otherwise calls save
     * @param		ReferenceGiver referenceGiver
     * @return		ReferenceGiver
     */
	@RequestMapping("/save")
    public ReferenceGiver save( @RequestBody ReferenceGiver referenceGiver )
    {
    	// assign locally
    	this.referenceGiver = referenceGiver;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a ReferenceGiver BO
     * @param		Long referenceGiverId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="referenceGiver.referenceGiverId", required=false) Long referenceGiverId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	ReferenceGiverBusinessDelegate delegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new ReferenceGiverPrimaryKey( referenceGiverId ) );
        		LOGGER.info( "ReferenceGiverController:delete() - successfully deleted ReferenceGiver with key " + referenceGiver.getReferenceGiverPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ReferenceGiverPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "ReferenceGiverController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferenceGiverController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a ReferenceGiver BO
     * @param		Long referenceGiverId
     * @return		ReferenceGiver
     */    
    @RequestMapping("/load")
    public ReferenceGiver load( @RequestParam(value="referenceGiver.referenceGiverId", required=true) Long referenceGiverId )
    {    	
        ReferenceGiverPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****ReferenceGiver.load pk is " + referenceGiverId );
        	if ( referenceGiverId != null )
        	{
        		pk = new ReferenceGiverPrimaryKey( referenceGiverId );
        		
        		// load the ReferenceGiver
	            this.referenceGiver = ReferenceGiverBusinessDelegate.getReferenceGiverInstance().getReferenceGiver( pk );
	            
	            LOGGER.info( "ReferenceGiverController:load() - successfully loaded - " + this.referenceGiver.toString() );             
			}
			else
			{
	            LOGGER.info( "ReferenceGiverController:load() - unable to locate the primary key as an attribute or a selection for - " + referenceGiver.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "ReferenceGiverController:load() - failed to load ReferenceGiver using Id " + referenceGiverId + ", " + exc.getMessage() );
            return null;
        }

        return referenceGiver;

    }

    /**
     * Handles loading all ReferenceGiver business objects
     * @return		List<ReferenceGiver>
     */
    @RequestMapping("/loadAll")
    public List<ReferenceGiver> loadAll()
    {                
        List<ReferenceGiver> referenceGiverList = null;
        
    	try
        {                        
            // load the ReferenceGiver
            referenceGiverList = ReferenceGiverBusinessDelegate.getReferenceGiverInstance().getAllReferenceGiver();
            
            if ( referenceGiverList != null )
                LOGGER.info(  "ReferenceGiverController:loadAllReferenceGiver() - successfully loaded all ReferenceGivers" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "ReferenceGiverController:loadAll() - failed to load all ReferenceGivers - " + exc.getMessage() );
        	return null;
            
        }

        return referenceGiverList;
                            
    }


// findAllBy methods


    /**
     * save QuestionGroup on ReferenceGiver
     * @param		Long	referenceGiverId
     * @param		Long	childId
     * @param		ReferenceGiver referenceGiver
     * @return		ReferenceGiver
     */     
	@RequestMapping("/saveQuestionGroup")
	public ReferenceGiver saveQuestionGroup( @RequestParam(value="referenceGiver.referenceGiverId", required=true) Long referenceGiverId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody ReferenceGiver referenceGiver )
	{
		if ( load( referenceGiverId ) == null )
			return( null );
		
		if ( childId != null )
		{
			QuestionGroupBusinessDelegate childDelegate 	= QuestionGroupBusinessDelegate.getQuestionGroupInstance();
			ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();			
			QuestionGroup child 							= null;

			try
			{
				child = childDelegate.getQuestionGroup( new QuestionGroupPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "ReferenceGiverController:saveQuestionGroup() failed to get QuestionGroup using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			referenceGiver.setQuestionGroup( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:saveQuestionGroup() failed saving parent ReferenceGiver - " + exc.getMessage() );
			}
		}
		
		return referenceGiver;
	}

    /**
     * delete QuestionGroup on ReferenceGiver
     * @param		Long referenceGiverId
     * @return		ReferenceGiver
     */     
	@RequestMapping("/deleteQuestionGroup")
	public ReferenceGiver deleteQuestionGroup( @RequestParam(value="referenceGiver.referenceGiverId", required=true) Long referenceGiverId )
	
	{
		if ( load( referenceGiverId ) == null )
			return( null );

		if ( referenceGiver.getQuestionGroup() != null )
		{
			QuestionGroupPrimaryKey pk = referenceGiver.getQuestionGroup().getQuestionGroupPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGiver.setQuestionGroup( null );
			try
			{
				ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();

				// save it
				referenceGiver = parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:deleteQuestionGroup() failed to save ReferenceGiver - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				QuestionGroupBusinessDelegate childDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:deleteQuestionGroup() failed  - " + exc.getMessage() );
			}
		}
		
		return referenceGiver;
	}
	
    /**
     * save User on ReferenceGiver
     * @param		Long	referenceGiverId
     * @param		Long	childId
     * @param		ReferenceGiver referenceGiver
     * @return		ReferenceGiver
     */     
	@RequestMapping("/saveUser")
	public ReferenceGiver saveUser( @RequestParam(value="referenceGiver.referenceGiverId", required=true) Long referenceGiverId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody ReferenceGiver referenceGiver )
	{
		if ( load( referenceGiverId ) == null )
			return( null );
		
		if ( childId != null )
		{
			UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
			ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();			
			User child 							= null;

			try
			{
				child = childDelegate.getUser( new UserPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "ReferenceGiverController:saveUser() failed to get User using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			referenceGiver.setUser( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:saveUser() failed saving parent ReferenceGiver - " + exc.getMessage() );
			}
		}
		
		return referenceGiver;
	}

    /**
     * delete User on ReferenceGiver
     * @param		Long referenceGiverId
     * @return		ReferenceGiver
     */     
	@RequestMapping("/deleteUser")
	public ReferenceGiver deleteUser( @RequestParam(value="referenceGiver.referenceGiverId", required=true) Long referenceGiverId )
	
	{
		if ( load( referenceGiverId ) == null )
			return( null );

		if ( referenceGiver.getUser() != null )
		{
			UserPrimaryKey pk = referenceGiver.getUser().getUserPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGiver.setUser( null );
			try
			{
				ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();

				// save it
				referenceGiver = parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:deleteUser() failed to save ReferenceGiver - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				UserBusinessDelegate childDelegate = UserBusinessDelegate.getUserInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:deleteUser() failed  - " + exc.getMessage() );
			}
		}
		
		return referenceGiver;
	}
	
    /**
     * save Referrer on ReferenceGiver
     * @param		Long	referenceGiverId
     * @param		Long	childId
     * @param		ReferenceGiver referenceGiver
     * @return		ReferenceGiver
     */     
	@RequestMapping("/saveReferrer")
	public ReferenceGiver saveReferrer( @RequestParam(value="referenceGiver.referenceGiverId", required=true) Long referenceGiverId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody ReferenceGiver referenceGiver )
	{
		if ( load( referenceGiverId ) == null )
			return( null );
		
		if ( childId != null )
		{
			ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
			ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();			
			Referrer child 							= null;

			try
			{
				child = childDelegate.getReferrer( new ReferrerPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "ReferenceGiverController:saveReferrer() failed to get Referrer using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			referenceGiver.setReferrer( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:saveReferrer() failed saving parent ReferenceGiver - " + exc.getMessage() );
			}
		}
		
		return referenceGiver;
	}

    /**
     * delete Referrer on ReferenceGiver
     * @param		Long referenceGiverId
     * @return		ReferenceGiver
     */     
	@RequestMapping("/deleteReferrer")
	public ReferenceGiver deleteReferrer( @RequestParam(value="referenceGiver.referenceGiverId", required=true) Long referenceGiverId )
	
	{
		if ( load( referenceGiverId ) == null )
			return( null );

		if ( referenceGiver.getReferrer() != null )
		{
			ReferrerPrimaryKey pk = referenceGiver.getReferrer().getReferrerPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGiver.setReferrer( null );
			try
			{
				ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();

				// save it
				referenceGiver = parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:deleteReferrer() failed to save ReferenceGiver - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				ReferrerBusinessDelegate childDelegate = ReferrerBusinessDelegate.getReferrerInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:deleteReferrer() failed  - " + exc.getMessage() );
			}
		}
		
		return referenceGiver;
	}
	
    /**
     * save LastQuestionAnswered on ReferenceGiver
     * @param		Long	referenceGiverId
     * @param		Long	childId
     * @param		ReferenceGiver referenceGiver
     * @return		ReferenceGiver
     */     
	@RequestMapping("/saveLastQuestionAnswered")
	public ReferenceGiver saveLastQuestionAnswered( @RequestParam(value="referenceGiver.referenceGiverId", required=true) Long referenceGiverId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody ReferenceGiver referenceGiver )
	{
		if ( load( referenceGiverId ) == null )
			return( null );
		
		if ( childId != null )
		{
			QuestionBusinessDelegate childDelegate 	= QuestionBusinessDelegate.getQuestionInstance();
			ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();			
			Question child 							= null;

			try
			{
				child = childDelegate.getQuestion( new QuestionPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "ReferenceGiverController:saveLastQuestionAnswered() failed to get Question using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			referenceGiver.setLastQuestionAnswered( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:saveLastQuestionAnswered() failed saving parent ReferenceGiver - " + exc.getMessage() );
			}
		}
		
		return referenceGiver;
	}

    /**
     * delete LastQuestionAnswered on ReferenceGiver
     * @param		Long referenceGiverId
     * @return		ReferenceGiver
     */     
	@RequestMapping("/deleteLastQuestionAnswered")
	public ReferenceGiver deleteLastQuestionAnswered( @RequestParam(value="referenceGiver.referenceGiverId", required=true) Long referenceGiverId )
	
	{
		if ( load( referenceGiverId ) == null )
			return( null );

		if ( referenceGiver.getLastQuestionAnswered() != null )
		{
			QuestionPrimaryKey pk = referenceGiver.getLastQuestionAnswered().getQuestionPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGiver.setLastQuestionAnswered( null );
			try
			{
				ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();

				// save it
				referenceGiver = parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:deleteLastQuestionAnswered() failed to save ReferenceGiver - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				QuestionBusinessDelegate childDelegate = QuestionBusinessDelegate.getQuestionInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:deleteLastQuestionAnswered() failed  - " + exc.getMessage() );
			}
		}
		
		return referenceGiver;
	}
	

    /**
     * save Answers on ReferenceGiver
     * @param		Long referenceGiverId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		ReferenceGiver
     */     
	@RequestMapping("/saveAnswers")
	public ReferenceGiver saveAnswers( @RequestParam(value="referenceGiver.referenceGiverId", required=false) Long referenceGiverId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( referenceGiverId ) == null )
			return( null );
		
		AnswerPrimaryKey pk 					= null;
		Answer child							= null;
		AnswerBusinessDelegate childDelegate 	= AnswerBusinessDelegate.getAnswerInstance();
		ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new AnswerPrimaryKey( childId );
			
			try
			{
				// find the Answer
				child = childDelegate.getAnswer( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGiverController:saveAnswers() failed get child Answer using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the Answers 
			referenceGiver.getAnswers().add( child );				
		}
		else
		{
			// clear out the Answers but 
			referenceGiver.getAnswers().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new AnswerPrimaryKey( id );
					try
					{
						// find the Answer
						child = childDelegate.getAnswer( pk );
						// add it to the Answers List
						referenceGiver.getAnswers().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "ReferenceGiverController:saveAnswers() failed get child Answer using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the ReferenceGiver
			parentDelegate.saveReferenceGiver( referenceGiver );
		}
		catch( Exception exc )
		{
			LOGGER.info( "ReferenceGiverController:saveAnswers() failed saving parent ReferenceGiver - " + exc.getMessage() );
		}

		return referenceGiver;
	}

    /**
     * delete Answers on ReferenceGiver
     * @param		Long referenceGiverId
     * @param		Long[] childIds
     * @return		ReferenceGiver
     */     	
	@RequestMapping("/deleteAnswers")
	public ReferenceGiver deleteAnswers( @RequestParam(value="referenceGiver.referenceGiverId", required=true) Long referenceGiverId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( referenceGiverId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			AnswerPrimaryKey pk 					= null;
			AnswerBusinessDelegate childDelegate 	= AnswerBusinessDelegate.getAnswerInstance();
			ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();
			Set<Answer> children					= referenceGiver.getAnswers();
			Answer child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new AnswerPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getAnswer( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "ReferenceGiverController:deleteAnswers() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of Answer back to the referenceGiver
			referenceGiver.setAnswers( children );			
			// save it 
			try
			{
				referenceGiver = parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "ReferenceGiverController:deleteAnswers() failed to save the ReferenceGiver - " + exc.getMessage() );
			}
		}
		
		return referenceGiver;
	}


    /**
     * Handles creating a ReferenceGiver BO
     * @return		ReferenceGiver
     */
    protected ReferenceGiver create()
    {
        try
        {       
			this.referenceGiver = ReferenceGiverBusinessDelegate.getReferenceGiverInstance().createReferenceGiver( referenceGiver );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferenceGiverController:create() - exception ReferenceGiver - " + exc.getMessage());        	
        	return null;
        }
        
        return this.referenceGiver;
    }

    /**
     * Handles updating a ReferenceGiver BO
     * @return		ReferenceGiver
     */    
    protected ReferenceGiver update()
    {
    	// store provided data
        ReferenceGiver tmp = referenceGiver;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	referenceGiver.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ReferenceGiverBusiness Delegate            
			ReferenceGiverBusinessDelegate delegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();
            this.referenceGiver = delegate.saveReferenceGiver( referenceGiver );
            
            if ( this.referenceGiver != null )
                LOGGER.info( "ReferenceGiverController:update() - successfully updated ReferenceGiver - " + referenceGiver.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferenceGiverController:update() - successfully update ReferenceGiver - " + exc.getMessage());        	
        	return null;
        }
        
        return this.referenceGiver;
        
    }


    /**
     * Returns true if the referenceGiver is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( referenceGiver != null && referenceGiver.getReferenceGiverPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected ReferenceGiver load()
    {
    	return( load( new Long( referenceGiver.getReferenceGiverPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected ReferenceGiver referenceGiver = null;
    private static final Logger LOGGER = Logger.getLogger(ReferenceGiver.class.getName());
    
}


