/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity ReferenceGroupLink.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/ReferenceGroupLink")
public class ReferenceGroupLinkRestController extends BaseSpringRestController
{

    /**
     * Handles saving a ReferenceGroupLink BO.  if not key provided, calls create, otherwise calls save
     * @param		ReferenceGroupLink referenceGroupLink
     * @return		ReferenceGroupLink
     */
	@RequestMapping("/save")
    public ReferenceGroupLink save( @RequestBody ReferenceGroupLink referenceGroupLink )
    {
    	// assign locally
    	this.referenceGroupLink = referenceGroupLink;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a ReferenceGroupLink BO
     * @param		Long referenceGroupLinkId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="referenceGroupLink.referenceGroupLinkId", required=false) Long referenceGroupLinkId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	ReferenceGroupLinkBusinessDelegate delegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new ReferenceGroupLinkPrimaryKey( referenceGroupLinkId ) );
        		LOGGER.info( "ReferenceGroupLinkController:delete() - successfully deleted ReferenceGroupLink with key " + referenceGroupLink.getReferenceGroupLinkPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ReferenceGroupLinkPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "ReferenceGroupLinkController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferenceGroupLinkController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a ReferenceGroupLink BO
     * @param		Long referenceGroupLinkId
     * @return		ReferenceGroupLink
     */    
    @RequestMapping("/load")
    public ReferenceGroupLink load( @RequestParam(value="referenceGroupLink.referenceGroupLinkId", required=true) Long referenceGroupLinkId )
    {    	
        ReferenceGroupLinkPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****ReferenceGroupLink.load pk is " + referenceGroupLinkId );
        	if ( referenceGroupLinkId != null )
        	{
        		pk = new ReferenceGroupLinkPrimaryKey( referenceGroupLinkId );
        		
        		// load the ReferenceGroupLink
	            this.referenceGroupLink = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance().getReferenceGroupLink( pk );
	            
	            LOGGER.info( "ReferenceGroupLinkController:load() - successfully loaded - " + this.referenceGroupLink.toString() );             
			}
			else
			{
	            LOGGER.info( "ReferenceGroupLinkController:load() - unable to locate the primary key as an attribute or a selection for - " + referenceGroupLink.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "ReferenceGroupLinkController:load() - failed to load ReferenceGroupLink using Id " + referenceGroupLinkId + ", " + exc.getMessage() );
            return null;
        }

        return referenceGroupLink;

    }

    /**
     * Handles loading all ReferenceGroupLink business objects
     * @return		List<ReferenceGroupLink>
     */
    @RequestMapping("/loadAll")
    public List<ReferenceGroupLink> loadAll()
    {                
        List<ReferenceGroupLink> referenceGroupLinkList = null;
        
    	try
        {                        
            // load the ReferenceGroupLink
            referenceGroupLinkList = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance().getAllReferenceGroupLink();
            
            if ( referenceGroupLinkList != null )
                LOGGER.info(  "ReferenceGroupLinkController:loadAllReferenceGroupLink() - successfully loaded all ReferenceGroupLinks" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "ReferenceGroupLinkController:loadAll() - failed to load all ReferenceGroupLinks - " + exc.getMessage() );
        	return null;
            
        }

        return referenceGroupLinkList;
                            
    }


// findAllBy methods


    /**
     * save ReferrerGroup on ReferenceGroupLink
     * @param		Long	referenceGroupLinkId
     * @param		Long	childId
     * @param		ReferenceGroupLink referenceGroupLink
     * @return		ReferenceGroupLink
     */     
	@RequestMapping("/saveReferrerGroup")
	public ReferenceGroupLink saveReferrerGroup( @RequestParam(value="referenceGroupLink.referenceGroupLinkId", required=true) Long referenceGroupLinkId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody ReferenceGroupLink referenceGroupLink )
	{
		if ( load( referenceGroupLinkId ) == null )
			return( null );
		
		if ( childId != null )
		{
			ReferrerGroupBusinessDelegate childDelegate 	= ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
			ReferenceGroupLinkBusinessDelegate parentDelegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();			
			ReferrerGroup child 							= null;

			try
			{
				child = childDelegate.getReferrerGroup( new ReferrerGroupPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "ReferenceGroupLinkController:saveReferrerGroup() failed to get ReferrerGroup using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			referenceGroupLink.setReferrerGroup( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGroupLink( referenceGroupLink );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGroupLinkController:saveReferrerGroup() failed saving parent ReferenceGroupLink - " + exc.getMessage() );
			}
		}
		
		return referenceGroupLink;
	}

    /**
     * delete ReferrerGroup on ReferenceGroupLink
     * @param		Long referenceGroupLinkId
     * @return		ReferenceGroupLink
     */     
	@RequestMapping("/deleteReferrerGroup")
	public ReferenceGroupLink deleteReferrerGroup( @RequestParam(value="referenceGroupLink.referenceGroupLinkId", required=true) Long referenceGroupLinkId )
	
	{
		if ( load( referenceGroupLinkId ) == null )
			return( null );

		if ( referenceGroupLink.getReferrerGroup() != null )
		{
			ReferrerGroupPrimaryKey pk = referenceGroupLink.getReferrerGroup().getReferrerGroupPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGroupLink.setReferrerGroup( null );
			try
			{
				ReferenceGroupLinkBusinessDelegate parentDelegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();

				// save it
				referenceGroupLink = parentDelegate.saveReferenceGroupLink( referenceGroupLink );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGroupLinkController:deleteReferrerGroup() failed to save ReferenceGroupLink - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				ReferrerGroupBusinessDelegate childDelegate = ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGroupLinkController:deleteReferrerGroup() failed  - " + exc.getMessage() );
			}
		}
		
		return referenceGroupLink;
	}
	
    /**
     * save LinkProvider on ReferenceGroupLink
     * @param		Long	referenceGroupLinkId
     * @param		Long	childId
     * @param		ReferenceGroupLink referenceGroupLink
     * @return		ReferenceGroupLink
     */     
	@RequestMapping("/saveLinkProvider")
	public ReferenceGroupLink saveLinkProvider( @RequestParam(value="referenceGroupLink.referenceGroupLinkId", required=true) Long referenceGroupLinkId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody ReferenceGroupLink referenceGroupLink )
	{
		if ( load( referenceGroupLinkId ) == null )
			return( null );
		
		if ( childId != null )
		{
			UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
			ReferenceGroupLinkBusinessDelegate parentDelegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();			
			User child 							= null;

			try
			{
				child = childDelegate.getUser( new UserPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "ReferenceGroupLinkController:saveLinkProvider() failed to get User using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			referenceGroupLink.setLinkProvider( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGroupLink( referenceGroupLink );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGroupLinkController:saveLinkProvider() failed saving parent ReferenceGroupLink - " + exc.getMessage() );
			}
		}
		
		return referenceGroupLink;
	}

    /**
     * delete LinkProvider on ReferenceGroupLink
     * @param		Long referenceGroupLinkId
     * @return		ReferenceGroupLink
     */     
	@RequestMapping("/deleteLinkProvider")
	public ReferenceGroupLink deleteLinkProvider( @RequestParam(value="referenceGroupLink.referenceGroupLinkId", required=true) Long referenceGroupLinkId )
	
	{
		if ( load( referenceGroupLinkId ) == null )
			return( null );

		if ( referenceGroupLink.getLinkProvider() != null )
		{
			UserPrimaryKey pk = referenceGroupLink.getLinkProvider().getUserPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGroupLink.setLinkProvider( null );
			try
			{
				ReferenceGroupLinkBusinessDelegate parentDelegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();

				// save it
				referenceGroupLink = parentDelegate.saveReferenceGroupLink( referenceGroupLink );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGroupLinkController:deleteLinkProvider() failed to save ReferenceGroupLink - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				UserBusinessDelegate childDelegate = UserBusinessDelegate.getUserInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceGroupLinkController:deleteLinkProvider() failed  - " + exc.getMessage() );
			}
		}
		
		return referenceGroupLink;
	}
	


    /**
     * Handles creating a ReferenceGroupLink BO
     * @return		ReferenceGroupLink
     */
    protected ReferenceGroupLink create()
    {
        try
        {       
			this.referenceGroupLink = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance().createReferenceGroupLink( referenceGroupLink );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferenceGroupLinkController:create() - exception ReferenceGroupLink - " + exc.getMessage());        	
        	return null;
        }
        
        return this.referenceGroupLink;
    }

    /**
     * Handles updating a ReferenceGroupLink BO
     * @return		ReferenceGroupLink
     */    
    protected ReferenceGroupLink update()
    {
    	// store provided data
        ReferenceGroupLink tmp = referenceGroupLink;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	referenceGroupLink.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ReferenceGroupLinkBusiness Delegate            
			ReferenceGroupLinkBusinessDelegate delegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();
            this.referenceGroupLink = delegate.saveReferenceGroupLink( referenceGroupLink );
            
            if ( this.referenceGroupLink != null )
                LOGGER.info( "ReferenceGroupLinkController:update() - successfully updated ReferenceGroupLink - " + referenceGroupLink.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferenceGroupLinkController:update() - successfully update ReferenceGroupLink - " + exc.getMessage());        	
        	return null;
        }
        
        return this.referenceGroupLink;
        
    }


    /**
     * Returns true if the referenceGroupLink is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( referenceGroupLink != null && referenceGroupLink.getReferenceGroupLinkPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected ReferenceGroupLink load()
    {
    	return( load( new Long( referenceGroupLink.getReferenceGroupLinkPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected ReferenceGroupLink referenceGroupLink = null;
    private static final Logger LOGGER = Logger.getLogger(ReferenceGroupLink.class.getName());
    
}


