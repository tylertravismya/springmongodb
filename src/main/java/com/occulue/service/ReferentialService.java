/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/

package com.occulue.service;

import java.util.*;

    import com.occulue.bo.*;
    
/**
 * Encapsulates data for business entity ReferentialService.
 * 
 * @author dev@realmethods.com
 */
// AIB : #getObjectClassDecl()
 public class ReferentialService 
{
// ~AIB

//************************************************************************
// Constructors
//************************************************************************

    /** 
     * Default Constructor 
     */
    public ReferentialService() 
    {
    }   

//************************************************************************
// Business Methods
//************************************************************************
// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() \$aib.getProxyTarget() true \$aib.usingSOAP() \$aib.usingCMP() )
// ~AIB
	
//************************************************************************
// Protected / Private Methods
//************************************************************************
    
//************************************************************************
// Attributes
//************************************************************************

// AIB : #getAttributeDeclarations( true  )
// ~AIB

}
