/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/**
 * Test ReferenceGroupLink class.
 *
 * @author    dev@realmethods.com
 */
public class ReferenceGroupLinkTest
{

// constructors

    public ReferenceGroupLinkTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a ReferenceGroupLink, through a ReferenceGroupLinkTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on ReferenceGroupLinkTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on ReferenceGroupLinkTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new ReferenceGroupLink.
     *
     * @return    ReferenceGroupLink
     */
    public ReferenceGroupLink testCreate()
    throws Throwable
    {
        ReferenceGroupLink businessObject = null;

    	{
	        LOGGER.info( "ReferenceGroupLinkTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a ReferenceGroupLink");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a ReferenceGroupLink" );
	
	        try 
	        {            
	            businessObject = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance().createReferenceGroupLink( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (ReferenceGroupLinkPrimaryKey)businessObject.getReferenceGroupLinkPrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a ReferenceGroupLink with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a ReferenceGroupLink.
     *
     * @return    ReferenceGroupLink  
     */
    public ReferenceGroupLink testRead()
    throws Throwable
    {
        LOGGER.info( "ReferenceGroupLinkTest:testRead()" );
        LOGGER.info( "-- Reading a previously created ReferenceGroupLink" );

        ReferenceGroupLink businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read ReferenceGroupLink with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance().getReferenceGroupLink( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found ReferenceGroupLink " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a ReferenceGroupLink.
     *
     * @return    ReferenceGroupLink
     */
    public ReferenceGroupLink testUpdate()
    throws Throwable
    {

        LOGGER.info( "ReferenceGroupLinkTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a ReferenceGroupLink." );

        StringBuilder msg = new StringBuilder( "Failed to update a ReferenceGroupLink : " );        
        ReferenceGroupLink businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created ReferenceGroupLink." );
            
            // for use later on...
            thePrimaryKey = (ReferenceGroupLinkPrimaryKey)businessObject.getReferenceGroupLinkPrimaryKey();
            
            ReferenceGroupLinkBusinessDelegate proxy = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();            
            businessObject = proxy.saveReferenceGroupLink( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved ReferenceGroupLink - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a ReferenceGroupLink.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "ReferenceGroupLinkTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created ReferenceGroupLink." );
        
        try
        {
            ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted ReferenceGroupLink with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete ReferenceGroupLink with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all ReferenceGroupLinks.
     *
     * @return    Collection
     */
    public ArrayList<ReferenceGroupLink> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "ReferenceGroupLinkTest:testGetAll() - Retrieving Collection of ReferenceGroupLinks:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all ReferenceGroupLink : " );        
        ArrayList<ReferenceGroupLink> collection  = null;

        try
        {
            // call the static get method on the ReferenceGroupLinkBusinessDelegate
            collection = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance().getAllReferenceGroupLink();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            ReferenceGroupLink currentBO  = null;            
	            Iterator<ReferenceGroupLink> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getReferenceGroupLinkPrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public ReferenceGroupLinkTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate ReferenceGroupLink
     * 
     * @return    ReferenceGroupLink
     */    
    protected ReferenceGroupLink getNewBO()
    {
        ReferenceGroupLink newBO = new ReferenceGroupLink();

// AIB : \#defaultBOOutput() 
newBO.setDateLinkCreated(
    java.util.Calendar.getInstance().getTime()
 );
newBO.setName(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected ReferenceGroupLinkPrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(ReferenceGroupLink.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}
